<?php
	
	foreach ($_SERVER['REQUEST_METHOD'] === 'POST' ?  $post : $get as $route => $handler) {
		$url = strpos($_SERVER['REQUEST_URI'], '?') !== false ? explode("?", $_SERVER['REQUEST_URI'])[0] : $_SERVER['REQUEST_URI'];
		if($route == $url) {
			$found = true;
			$controller = explode("@", $handler)[0];
			$method = explode("@", $handler)[1];
			require("../controllers/" . $controller . ".php");
		}
	}

	if(!isset($found)) {
		header('location:/');
	}

?>