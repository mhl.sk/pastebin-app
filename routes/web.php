<?php
	
	$get = [
		'/' => "MainController@index",
		'/open' => "MainController@open",
		'/generate' => "MainController@generate"
	];

	$post = [
		'/create' => "MainController@create",
		'/save' => "MainController@save"
	];

?>