<?php include("partials/header.php"); ?>

<main class="main-open">
	<div class="container">
		<h5 class="pt-4 pt-lg-5">
			<span>
				INFO:
			</span> 
			<span class="font-weight-normal">
				<span>This message has been deleted</span> 
				<span class="d-none d-lg-inline">and it can't be loaded again</span>
			</span>
		</h5>
		<hr>
		<div class="main-open-message-holder">
			<?php echo $message ?>
		</div>
		<button 
			class="main-open-close-btn"
			onclick="location.reload()"
		>
			<i class="fas fa-eye-slash mr-2"></i>
			<span>Close this message</span>
		</button>
	</div>
</main>

<?php include("partials/footer.php"); ?>