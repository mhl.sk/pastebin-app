<!DOCTYPE html>
<html lang="en">
<head>
	
	<title>Self-hosted Pastebin</title>

	<meta charset="UTF-8">

	<meta 
		name="viewport"
		content="width=device-width, initial-scale=1.0"
	>

	<link 
		rel="icon" 
		type="image/png" 
		href="favicon.png" 
	/>
	
	<link 
		rel="stylesheet"
		href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
		integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"
		crossorigin="anonymous"
	>

	<link
		rel="stylesheet"
		href="https://use.fontawesome.com/releases/v5.13.1/css/all.css"
		integrity="sha384-xxzQGERXS00kBmZW/6qxqJPyxW3UR0BPsL4c8ILaIWXva5kFi7TxkIIaMiKtqV1Q"
		crossorigin="anonymous"
	/>

	<script
		src="https://cdn.jsdelivr.net/npm/autosize@4.0.2/dist/autosize.min.js"
	></script>

	<link 
		rel="stylesheet"
		href="/css/style.css"
	>

</head>
<body>