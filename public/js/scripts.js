autosize($('textarea'));

function generate(element) {
    new ClipboardJS('.main-create-generate');
 	$.ajax({
        type: 'POST',
        url: '/save',
        data: { 
            'uuid': $('#uuid').val(),
            'text': $("#text").val()
        },
        success : function(data) {
        	if(data == "success") {
                location.reload();
            } else {
                alert("Error occured, reload the page and try it again");
            }
        } 
    });
}

$(".message-send-it").click(function(){
    setTimeout(function(){
        console.clear();
    }, 1000); 
});

$(".result-message").delay(500).fadeIn().delay(10000).fadeOut();

if(window.history.replaceState) {
    window.history.replaceState( null, null, window.location.href );
}