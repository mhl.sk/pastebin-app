<?php
	
	use Medoo\Medoo;
	
	$credentials = file_get_contents("../credentials.json");
	$credentials = json_decode($credentials);

	$database = new Medoo([
	    'database_type' => 'mysql',
	    'database_name' => $credentials->db_name,
	    'server' 		=> $credentials->db_host,
	    'username' 		=> $credentials->db_username,
	    'password' 		=> $credentials->db_password
	]);

	$encryption = $credentials->encryption;
	$password = $credentials->app_password;
	$auth_name = $credentials->auth_name;
	$auth_url = $credentials->auth_url;
		
?>