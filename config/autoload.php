<?php 
	
	$ignored = [".", "..", "autoload.php"];

	foreach (scandir(__DIR__) as $config) {
		if(!in_array($config, $ignored)) {
			require_once ("../config/" . $config);
		}
	}

?>